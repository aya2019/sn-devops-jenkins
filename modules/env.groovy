
def getEnvValue(def envName){
 String value = ""
    try{
      value = env[envName]
     }
     catch(Exception e) {
      error "environement value  ${envName} not exist"
      throw e
    }
    return value
 }

def getWorkspace(){
   return getEnvValue("WORKSPACE")

}

return this