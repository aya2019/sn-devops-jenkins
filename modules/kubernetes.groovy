def kubernetesCreateConfigMap(String nameConfigMap, String pathToConfig) {
        
     echo "Deploy configmap via Kubernetes"        
      ansiColor('xterm') {
            sh(script: "kubectl create configmap ${nameConfigMap} " +
                    "-from-file=${pathToConfig} " +
                    "--namespace=${env}" 
            )
        }
    
}


def kubernetesCreateController(String pathToDeploymentFile) {
        
     echo "Deploy controller via Kubernetes"        
      ansiColor('xterm') {
            sh(script: "kubectl create -f ${pathToDeploymentFile} " +
                "--namespace=${env}"             
            )
        }
    
}

def kubernetesCreateNameSpace(String pathToDeploymentFile) {
        
      echo "Deploy Namespace via Kubernetes"        
      ansiColor('xterm') {
          sh(script: "kubectl create -f ${pathToDeploymentFile} "             
           )
      }
    
}

return this