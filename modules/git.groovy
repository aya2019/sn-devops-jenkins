modules = null
def isMaster(){

    return getBranchName().equal('master')
}

def getBranchName(){

    return modules.env().getEnvValue("BRANCH_NAME")
}

def isBugFix(){

    return getBranchName().contains('bugfix')
}

def checkout(String url, String branchName) {
    checkout([$class                           : 'GitSCM', branches: [[name: "${branchName}"]],
              doGenerateSubmoduleConfigurations: false,
              localBranch                      : "${branchName}",   
              submoduleCfg                     : [],
              userRemoteConfigs                : [[credentialsId: 'aya',
                                                   url: "${url}"]]])
}


return this