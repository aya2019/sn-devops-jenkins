private void doTerraformPlanApply(String version, String composantName, String env, String region, String companyName, String projectName, String deployName) {
        bucket = "els-${env}-tfstate-${region}"
        sh "ls -ll"
        echo "Deploy Stack for project ${projectName} via Terraform"
        terraformInit("${env}/${companyName}/${projectName}/${deployName}/${composantName}/terraform.tfstate", region, bucket)
        Map vars = [composant:"${composantName}", env:"${env}", version:"${version}", deploy:"${deployName}", companyName:"${companyName}", region:"${region}"]
        terraformApply(vars)
    
}


def terraformInit(String tfState, String region, String bucket) {
        ansiColor('xterm') {
            sh(script: "terraform init " +
                    "-backend=true " +
                    "-backend-config='bucket=${bucket}' " +
                    "-backend-config='key=${tfState}' " +
                    "-backend-config='region=${region}' " +
                    "-input=false " +
                    "-force-copy"
            )
        }
    
}

def terraformApply(Map paramsJob) {
    
    Map finalVars = [:]
    finalVars << paramsJob

    def shScript = "terraform apply -input=false -auto-approve"
    for (entry in finalVars) {
        shScript += " -var '${entry.key}=${entry.value}'"
    }

    ansiColor('xterm') {
        shScript +=  " -var-file='../live/${finalVars.composant}/${finalVars.env}/terraform.tfvars'"
        sh(script: shScript)
    }
    
}

def destroyStack(String url, String projectName, String deployName, String composantName, String env, String version, String region, String companyName) {

        bucket = "els-${env}-tfstate-${region}"
        terraformInit("${env}/${companyName}/${projectName}/${deployName}/${composantName}/terraform.tfstate","${region}","${bucket}")
        destroy(version, composantName, env)
}


def destroy(String projectVersion, String composant, String env) {
        ansiColor('xterm') {
            sh(script: "terraform destroy " +
                    "-var 'new_app_version=${projectVersion}' " +
                    "-var 'env=${env}' " +
                    "-var-file='../live/${composant}/${env}/terraform.tfvars' " +
                    "-auto-approve")
        }
    
}

return this