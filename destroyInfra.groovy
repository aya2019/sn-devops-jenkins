 url = "https://bitbucket.org/elsgestion/ito-devops-terraform-module.git"
 projectName = params.PROJECT_NAME
 deployName = params.DEPLOY_NAME
 companyName = params.COMPANY_NAME
 envName = params.ENV
 branch_name= params.BRANCH_NAME
 version= params.VERSION
 region= params.REGION
 vpc= params.VPC
 iam= params.ROLES
 modules = null

 node('slave_linux_sisn') {
  disableConcurrentBuilds()
  gitCheckout()
    unstash 'shareRepoWs'
    stage('checkout terraform project'){
      
      dir("terraformDir-${params.ENV}"){
        modules.git().checkout(url,branch_name)
      }
      stash includes: "terraformDir-${params.ENV}/**" , name: 'terraformWS'

  }    
  node('aws_bastion_linux_sisn') {
    stage('approve destroy') {
     input 'Do you approve destroy infra aws?'
    }
    stage('destroy infra aws') {
      unstash 'shareRepoWs'
      unstash 'terraformWS'  
	     // call method destroy terraform
      withCredentials([
	 		 [$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID',
	 					credentialsId: 'bastion-sisn', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']
	     ]) {
        
        dir("sharelibeDir") {
          loadModules()
        }
          echo "Destroy a stack for branch ${branch_name} via Terraform"  
          if ( "${vpc}" == "true"){
            dir("terraformDir-${params.ENV}/vpc") {
               modules.terraform().destroyStack(url, projectName, deployName, "vpc", envName, version, region, companyName)
            }
          }
           if ( "${iam}" == "true"){
            dir("terraformDir-${params.ENV}/setup-account") {
               modules.terraform().destroyStack(url, projectName, deployName, "setup-account", envName, version, region, companyName)
            }
          }
           
        }
      }
    }    
 } 

 def loadModules() {
  def lib = "load.groovy"
  this.modules = load lib
 }

def gitCheckout() {
  stage('prepare') {
  dir("sharelibeDir") {
    checkout scm
    loadModules()
  }
     stash includes: "sharelibeDir/**" , name: 'shareRepoWs'
 
 }

}
