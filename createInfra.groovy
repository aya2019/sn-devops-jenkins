 terraformUrl = params.TERRAFORM_REPO_URL
 projectName = params.PROJECT_NAME
 deployName = params.DEPLOY_NAME
 composantNameList = params.COMPOSANTS
 envName = params.ENV
 branch_name= params.BRANCH_NAME
 version= params.VERSION
 region= params.REGION
 modules = null

 node('slave_linux_sisn') {
  gitCheckout()
   stage('checkout terraform project'){
      unstash 'shareRepoWs'
      dir("terraformDir-${params.ENV}") {
        modules.git().checkout(terraformUrl,branch_name)
      }
       stash includes: "terraformDir-${params.ENV}/**" , name: 'terraformWS'  
    }
    
  node('aws_bastion_linux_sisn') {

    stage('approve deploy') {
     input 'Do you approve deploy infra aws?'
    }
    stage('deploy infra aws') {
      unstash 'shareRepoWs'
      unstash 'terraformWS'

	     // call method Deploy terraform
      withCredentials([
	 		 [$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID',
	 					credentialsId: 'bastion-sisn', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']
	     ]) {
        
        dir("sharelibeDir") {
          loadModules()
                     
        }
      
        dir("terraformDir-${params.ENV}") {
        echo "Deploy a stack for branch ${branch_name} via Terraform"
        
          String[] composantNameList_ = composantNameList.split(',')
          for (int i=0 ; i< composantNameList_.length; i++) {
            String composant = composantNameList_[i].trim()
             dir(composant){
               modules.terraform().doTerraformPlanApply(version, composant, envName, region, "", projectName, deployName)
             }
          }
        }
      }
    }
  }
    
 } 

def loadModules() {
      
  def lib = "load.groovy"
  this.modules = load lib    
       
}

def gitCheckout() {
 stage('prepare') {
  dir("sharelibeDir") {
    checkout scm
    loadModules()
  }
     stash includes: "sharelibeDir/**" , name: 'shareRepoWs'
 
 }
 
}

