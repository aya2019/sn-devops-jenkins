 kubernetesUrl = params.KUBERNETES_REPO_URL
 envName = params.ENV
 branch_name= params.BRANCH_NAME
 version= params.VERSION
 imageDocker= params.IMAGE_DOCKER
 nameConfigMap= params.CONFIG_MAP
 pathToConfig= params.PATH_TO_CONFIG
 modules = null

 node('slave_linux_sisn') {
  gitCheckout()
   stage('checkout kubernetes project'){
      unstash 'shareRepoKub'
      dir("kubernetesDir-${envName}") {
        modules.git().checkout(kubernetesUrl,branch_name)
      }
       stash includes: "kubernetesDir-${envName}/**" , name: 'kubernetes'  
    }
    
  node('aws_bastion_linux_sisn') {

    stage('approve deploy') {
     input 'Do you approve deploy k8s ?'
    }
    stage('deploy app K8s') {
      unstash 'shareRepoKub'
      unstash 'kubernetes'
      withCredentials([
	 		 [$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID',
	 					credentialsId: 'bastion-sisn', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']
	     ]) {
        
        dir("sharelibeDir") {
          loadModules()    
        }
      
        dir("kubernetesDir-${envName}") {         
           modules.kubernetes().kubernetesCreateNameSpace(pathToDeploymentFile)
      }
    }
  }
    
 } 

def loadModules() {
      
  def lib = "load.groovy"
  this.modules = load lib    
       
}

def gitCheckout() {
 stage('prepare') {
  dir("sharelibeDir") {
    checkout scm
    loadModules()
  }
     stash includes: "sharelibeDir/**" , name: 'shareRepoKub'
 
 }
 
}

