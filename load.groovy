
pwdPath = pwd()
rootPath = "${pwdPath}/modules"
gitFile = 'git'
envFile = 'env'
terraformFile= 'terraform'
kubernetes= 'kubernetes'


def loadFile(def scriptName){

def path = "${rootPath}/${scriptName}.groovy"

 if ( scriptName == null) {

    error 'script $scriptName doest not exist in $rootPath: check your script name'

} 

 def script = load path.toString()
 script.modules = this
 return script
}

def git(){
     loadFile(gitFile)
}


def env(){
     loadFile(envFile)
}

def terraform(){
     loadFile(terraformFile)
}

def kubernetes(){
     loadFile(kubernetes)
}

return this