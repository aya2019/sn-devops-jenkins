 terraformUrl = "https://bitbucket.org/elsgestion/ito-devops-terraform-module.git"
 projectName = params.APP_NAME
 deployName = params.DEPLOY_NAME
 companyName = params.COMPANY_NAME
 envName = params.ENV
 branch_name= params.BRANCH_NAME
 version= params.VERSION
 region= params.REGION
 vpc= params.VPC
 roles= params.ROLES
 modules = null

 node('slave_linux_sisn') {
  disableConcurrentBuilds()

  gitCheckout()
   stage('checkout terraform project'){
      unstash 'shareRepoWs'
      dir("terraformDir-${params.ENV}") {
        modules.git().checkout(terraformUrl,branch_name)
      }
       stash includes: "terraformDir-${params.ENV}/**" , name: 'terraformWS'  
    }
    
  node('aws_bastion_linux_sisn') {

    stage('approve deploy') {
     input 'Do you approve deploy infra aws?'
    }
    stage('deploy infra aws') {
      unstash 'shareRepoWs'
      unstash 'terraformWS'
	     
      withCredentials([
	 		 [$class: 'AmazonWebServicesCredentialsBinding', accessKeyVariable: 'AWS_ACCESS_KEY_ID',
	 					credentialsId: 'bastion-sisn', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']
	     ]) {
        
        dir("sharelibeDir") {
          loadModules()          
        }
        
        if ( "${vpc}" == "true") {
          dir("terraformDir-${params.ENV}/vpc") {
            echo "Deploy a stack vpc for branch ${branch_name} via Terraform"
            modules.terraform().doTerraformPlanApply(version, "vpc", envName, region, companyName, projectName, deployName)
         }
        }
        if ( "${roles}" == "true"){
          dir("terraformDir-${params.ENV}/setup-account") {
            echo "Deploy a stack role for branch ${branch_name} via Terraform"
            modules.terraform().doTerraformPlanApply(version, "iam", envName, region, companyName, projectName, deployName)
         }
       }
        
      }
    }
  }
    
 } 

def loadModules() {
  def lib = "load.groovy"
  this.modules = load lib    
}

def gitCheckout() {
 stage('prepare') {
  dir("sharelibeDir") {
    checkout scm
    loadModules()
  }
     stash includes: "sharelibeDir/**" , name: 'shareRepoWs'
 }
 
}

